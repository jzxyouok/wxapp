//app.js
App({
	onLaunch: function () {
		//程序启到时，直接跳转到应用模块中
		wx.redirectTo({
			url: '/ewei_hotel/pages/index/index'
		})
	},
	onShow: function () {
	},
	onHide: function () {
	},
	onError: function (msg) {
		console.log(msg)
	},
	//加载微擎工具类
	util: require('we7/resource/js/util.js'),
	//导航菜单，微擎将会自己实现一个导航菜单，结构与小程序导航菜单相同
	tabBar: {
		"color": "#123",
		"selectedColor": "#1ba9ba",
		"borderStyle": "#1ba9ba",
		"backgroundColor": "#fff",
		"list": [
			{
				"pagePath": "/ewei_hotel/pages/index/index",
				"iconPath": "/we7/resource/icon/hotel.png",
				"selectedIconPath": "/we7/resource/icon/hotelselect.png",
				"text": "微酒店"
			},
			{
				"pagePath": "/ewei_hotel/pages/orderlist/orderlist",
				"iconPath": "/we7/resource/icon/order.png",
				"selectedIconPath": "/we7/resource/icon/orderselect.png",
				"text": "订单"
			},
		]
	},
	//用户信息，sessionid是用户是否登录的凭证
	userInfo: {
		sessionid: null,
	},
	//站点信息
	siteInfo: {
		'uniacid': '8641', //公众号uniacid
		'acid': '8650', 
		'multiid': '8907',  //小程序版本id
		'version': '1.0.0',  //小程序版本
		'siteroot': 'https://pro.we7.cc/app/index.php',  //站点URL
		'token': 12312 //将用于接口中的数据安全校验
	}
});