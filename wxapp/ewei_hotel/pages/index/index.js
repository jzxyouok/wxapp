//index.js
//获取应用实例
var js = require('../../resource/js/app.js')
var app = getApp()
Page({
	data: {
		userInfo: {},
		hotelList: {},
		inputShowed: false,
		inputVal: ""
	},
	//事件处理函数
	onLoad: function () {
		var that = this
		app.util.getUserInfo(function () {
			js.getHotelList(function (res) {
				that.setData({
					hotelList: res.data.data.hotels
				})
			});
		});
		app.util.footer(that);
		//调用应用实例的方法获取全局数据
	},
	img: function () {
		console.log(1);
	}
})
