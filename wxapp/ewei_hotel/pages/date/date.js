var app = getApp();
Page({
    data: {
        strat_date: app.util.formatDate(new Date),
        search_array: {
            bdate: null,
            day: 1
        },
        hid: null
    },
    onLoad: function(option) {
        var that = this ;
        that.setData({
            hid: option.hid
        })
        wx.getStorage({
            key: 'ewei_hotel_search_array',
            success: function(res) {
                if(res.data) {
                    that.setData({
                        search_array: res.data
                    })
                }
            }
        })
    },
    bindDateChange: function(e) {
        this.setData({
            'search_array.bdate': e.detail.value
        })
    },
    bindDayChange: function(e) {
        this.setData({
            'search_array.day': Number(e.detail.value)
        })
    },
    input_subtract: function() {
        this.setData({
            'search_array.day': (this.data.search_array.day - 1 > 1) ? this.data.search_array.day - 1 : 1
        }) 
    },
    input_add: function() {
        this.setData({
            'search_array.day': this.data.search_array.day + 1
        })   
    },
    submit: function() {
        wx.showToast({
            title: '保存中',
            icon: 'loading',
            duration: 10000
        })
        var hid =  this.data.hid;
        wx.setStorage({
            key: "ewei_hotel_search_array",
            data: this.data.search_array,
            success: function() {
                wx.hideToast();
                wx.redirectTo({
                    url: '/ewei_hotel/pages/list/list?hid=' + hid
                })
            },
            fail: function() {
            }
        })
    }
})