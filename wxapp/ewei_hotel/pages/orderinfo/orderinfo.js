var app = getApp();
var js = require('../../resource/js/app.js');
Page({
	data: {
		text: "Page orderinfo"
	},
	onLoad: function (options) {
		// 页面初始化 options为页面跳转所带来的参数
		var that = this;
		app.util.footer(that);
		app.util.request({
			url: 'entry/wxapp/OrderDetail',
			data: {
				m: 'ewei_hotel',
				order_id: options.order_id
			},
			cachetime: 0,
			success: function (res) {
				if (!res.data.errno) {
					that.setData({
						item: res.data.data.item,
						success: options.order ? 1 : 0
					})
				} else {
					js.failGo(res.data.message);
				}
			},
			fail: function () {
				js.failGo(res.data.message);
			}
		})
	},
	onReady: function () {
		// 页面渲染完成
	},
	onShow: function () {
		// 页面显示
	},
	onHide: function () {
		// 页面隐藏
	},
	onUnload: function () {
		// 页面关闭
	},
	cancelOrder: function () {
		var that = this;
		wx.showModal({
			titel: '取消订单',
			content: '确定要取消订单？',
			success: function (res) {
				if (res.confirm) {
					app.util.request({
						url: 'entry/wxapp/CancelOrder',
						data: {
							m: 'ewei_hotel',
							order_id: that.data.item.id
						},
						success: function (res) {
							if (res.data.errno == 0) {
								wx.showToast({
									title: res.data.message,
									icon: 'success'
								}),
									setTimeout(function () {
										that.setData({
											item: res.data.data.item
										})
									}, 1000)
							}
						}
					})
				}
			}
		})
	},
	orderPay: function () {
		var that = this;
		app.util.request({
			url: 'entry/wxapp/OrderPay',
			data: {
				m: 'ewei_hotel',
				order_id: that.data.item.id
			},
			success: function (res) {
				if (res.data.errno == 0 && res.data.data) {
					console.log(res);
					wx.requestPayment({
						'timeStamp': new Date().getTime(),
						'nonceStr': '',
						'package': '',
						'signType': 'MD5',
						'paySign': '',
						'success': function (res) {
						},
						'fail': function (res) {
						}
					})
				}
			}
		})
	}
})