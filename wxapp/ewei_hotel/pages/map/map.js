var app = getApp();
Page({
    data: {
        latitude: null,
        longitude: null,
        markers: [{
            //iconPath: "/resources/others.png",
            id: 0,
            latitude: null,
            longitude: null,
            width: 50,
            height: 50
        }]
    },
    onLoad: function(option) {
        var latitude = Number(option.latitude)
        var longitude = Number(option.longitude)
        if(latitude && longitude) {
            this.setData({
               latitude: latitude,
               longitude: longitude,
               'markers.latitude': latitude,
               'markers.longitude': longitude
            })   
        } else {
             wx.showModal({
                title: '无坐标',
                content: '找不到改地址',
                showCancel: false,
                success: function(res){
                    if(res.confirm) {
                        console.log(1);
                        wx.switchTab({
                            url: 'index'
                        })
                    }
                }
            });
        }
    }
})